#include <stdio.h>
#include <string.h>
#include <malloc.h>


typedef struct no {
      
       char bone[30];
       struct no *proximo;
      
       } no;
      
no *topo = NULL; 
no *alocar; 



void push(char *bone) {
    
    alocar = (struct no *) malloc(sizeof(struct no)); 
        
    if (!alocar) { 
       printf("Falta de memória");
       exit(0);
    }
    
    strcpy(alocar->bone, bone); 
    
     if (!topo) { 
         topo = alocar;
         topo->proximo = NULL;
        
     }
    
     else 
     {
         alocar->proximo = topo; 
         topo = alocar;          
        
     }
}


char * pop() {
    
    char *ponteiro = topo->bone;
    topo = topo->proximo;
    return ponteiro;

    
    }


void imprimir() {
     no *ponteiro = topo;
     while (ponteiro) {
           printf("\n%s", ponteiro->bone);
           ponteiro = ponteiro->proximo;
          }
     }

     
int main() {
   char bone[30];
    printf("\nInserir: ");
    gets(bone);
    push(bone);
    printf("\nInserir: ");
    gets(bone);
    push(bone);
    printf("\nInserir: ");
    gets(bone);
    push(bone);
    printf("\nInserir: ");
    gets(bone);
    push(bone);
    imprimir();
    printf("\nRetirar: ");
    getchar();
    
    imprimir();
    printf("\nRetirar: ");
    getchar();
    printf("\nSaiu da pilha: %s", pop());
    imprimir();
    getchar();
}